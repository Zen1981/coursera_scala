package week4

// Pattern matching
// A case class definition is like a normal definition of a class preceded by case
// This add companion objects to the classes containing factory methods that construct the class automatically
//
// object Number {
//    def apply(n: Int) = new Number(n)
// }
//
// so we can write just Number(1) for example

trait Expr
case class Number(n: Int) extends Expr
case class Sum(e1: Expr, e2: Expr) extends Expr