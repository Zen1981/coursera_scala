package week4

// Here we define a method show that behaves different
// depending on the type of its argument.
// Rules:
//    · The first pattern found is applied
//    · The pattern can be: Constructors, variables, constants
//    · Wildcard _ can be used to match anything as an argument (we won't need reference this argument)
//    · A constant name can be used, in which case we should use capital letters for the name)
//    · A variable can be used too, but then we should use an _ as a prefix for its name (except for null, true and false)
//    · The same variable name can appear JUST ONCE in the same pattern

object exprs {
  def show(e: Expr): String = e match {
    case Number(x) => x.toString
    case Sum(l, r) => show(l) + "+" + show(r)
  }

  show(Sum(Number(1), Number(44)))
}