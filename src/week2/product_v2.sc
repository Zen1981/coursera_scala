object exercise1 {
  // Part 1 : write a function product analogous to sum
  def product(f: Int => Int)(start: Int, end: Int): Int =
    if (start > end) 1
    else f(start) * product(f)(start + 1, end)

  product(x => x * x)(3, 7)

  // Part 2 : define factorial in terms of product
  def factorial(a: Int): Int =
    product(x => x)(1, a)

  factorial(5)

  // Part 3 : Write a function that generalize sum and product
  def mapReduce(f: Int => Int, combine: (Int, Int) => Int, zero: Int)(a: Int, b: Int): Int =
    if (a > b) zero
    else combine(f(a), mapReduce(f, combine, zero)(a + 1, b))

  def product2(f: Int => Int)(start: Int, end: Int): Int =
    mapReduce(f, (x, y) => x * y, 1)(start, end)

  product2(x => x * x)(3, 7)
}