// Sum can be written as follows in scala.
// Definition of functions that returns functions is so useful that can be written in a special syntax
// Parameter lists of inner and outer functions are concatenated
def sum(f: Int => Int)(a: Int, b: Int): Int =
  if (a > b) 0 else f(a) + sum(f)(a + 1, b)

sum(x => x * x)(1, 5)

// Without evaluating, we can obtain a function for later use
// sum(x => x * x)