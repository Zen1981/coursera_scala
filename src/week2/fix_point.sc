import math.abs

object exercise {
  val tolerance = 0.0001

  def isCloseEnough(x: Double, y: Double) =
    abs((x - y) / x) / x < tolerance

  def fixedPoint(f: Double => Double)(firstGuess: Double) = {
    def iterate(guess: Double): Double = {
//      println("guess = " + guess)
      val next = f(guess)
      if (isCloseEnough(guess, next)) next
      else iterate(next)
    }
    iterate(firstGuess)
  }

  fixedPoint(x => 1 + x / 2)(1)

  // Square root(x) = y --> y * y = x
  // If we divide both sides by y --> y = x / y
  // y is a fixed point of function y = x / y
  def sqrt(x: Double) = fixedPoint(y => (y + x / y) / 2)(1)

  sqrt(2)
}


