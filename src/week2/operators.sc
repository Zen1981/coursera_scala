// Infix notation:
// Any method with a parameter can be used like an infix operator
// r add s === r.add(s)
// To achieve this, identifiers can be operators in scala

class Rational(x: Int, y: Int) {
  // Precondition that must be true.
  // It is possible to use assert can be used too but to enforce a precondition on the caller it's more correct to use require
  require(y != 0, "Denominator can't be 0")

  // Definition of a secondary constructor that uses primary constructor
  def this(x: Int) = this(x, 1)

  private def gcd(a: Int, b: Int): Int =
    if (b == 0) a else gcd(b, a % b)

  def g = gcd(x, y)

  // Numerator and denominator could be defined with val, which is better if they are invoked a lot of times
  def numerator = x / g

  def denominator = y / g

  def <(that: Rational) = numerator * that.denominator < that.numerator * denominator

  def max(that: Rational) = if (this < that) that else this

  def +(that: Rational) =
    new Rational(numerator * that.denominator + that.numerator * denominator, denominator * that.denominator)

  //
  def unary_- : Rational = new Rational(-numerator, denominator)

  def -(z: Rational) = this + -z

  override def toString = numerator + "/" + denominator
}

val x = new Rational(2, 3)
val y = new Rational(1, 7)

x - y
x + y
x < y
x unary_-