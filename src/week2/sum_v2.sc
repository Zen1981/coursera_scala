def sum(f: Int => Int): (Int, Int) => Int = {
  def sumF(start: Int, end: Int): Int =
    if (start > end) 0
    else f(start) + sumF(start + 1, end)
  sumF
}

def sumInts = sum(x => x)

def sumCubes = sum(x => x * x * x)

def sumFactorials = sum(fact)

def fact(a: Int): Int =
  if (a == 0) 1 else a * fact(a - 1)

fact(5)
sumCubes(1, 10)
sumFactorials(10, 20)

// We don't need sumCubes definition
// Here, sum(...) returns a function sumF that is evaluated with arguments 1 and 10
sum(x => x * x * x)(1, 10)