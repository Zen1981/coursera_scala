class Rational(x: Int, y: Int) {
  // Precondition that must be true.
  // It is possible to use assert can be used too but to enforce a precondition on the caller it's more correct to use require
  require(y != 0, "Denominator can't be 0")

  // Definition of a secondary constructor that uses primary constructor
  def this(x: Int) = this(x, 1)

  private def gcd(a: Int, b: Int): Int =
    if (b == 0) a else gcd(b, a % b)

  def g = gcd(x, y)

  // Numerator and denominator could be defined with val, which is better if they are invoked a lot of times
  def numerator = x / g

  def denominator = y / g

  def less(that: Rational) = numerator * that.denominator < that.numerator * denominator

  def max(that : Rational) = if (this.less(that)) that else this

  def add(that: Rational) =
    new Rational(numerator * that.denominator + that.numerator * denominator, denominator * that.denominator)

  def neg: Rational = new Rational(-numerator, denominator)

  def subtract(z: Rational) = this.add(z.neg)

  override def toString = numerator + "/" + denominator
}

// Why this object body is not evaluated? in video it is...
object rationals {
  val x = new Rational(1, 3)
  x.numerator
  x.denominator
  x.neg

  val y = new Rational(5, 7)
  x.add(y)
  x.subtract(y)

  val z = new Rational(3, 2)
  x.subtract(y).subtract(z)
}

val x = new Rational(1, 3)
x.numerator
x.denominator
x.neg

val y = new Rational(5, 7)
x.add(y)
x.subtract(y)

val z = new Rational(3, 2)
x.subtract(y).subtract(z)

y.add(y)

x.less(y)

x.max(y)

//val strange = new Rational(1,0)
//strange.add(strange)

new Rational(2)