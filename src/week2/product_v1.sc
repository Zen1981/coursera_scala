def product(f: Int => Int): (Int, Int) => Int = {
  def productF(a: Int, b: Int): Int =
    if (a > b) 1 else f(a) * productF(a + 1, b)
  productF
}

product(x => x)(1, 3)