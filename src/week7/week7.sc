import week7.Pouring

/**
 * Streams
 */
// Construction
Stream.cons(1, Stream.empty)
1 #:: Stream.empty

// Evaluation
def streamRange(lo: Int, hi: Int): Stream[Int] = {
  print(lo + " ")
  if (lo >= hi) Stream.empty
  else Stream.cons(lo, streamRange(lo + 1, hi))
}

streamRange(1,3)
streamRange(1,10).take(3).toList  // A list must be fully evaluated, can't have ?

/**
 * Lazy evaluation
 */
def expr = {
  val x = { print("x"); 1}        // It's evaluated once immediately
  lazy val y = { print("y"); 2}   // It's evaluated once when y is requested
  def z = { print("z"); 3}        // It's evaluated everytime z is requested
  z + y + x + z + y + x
}
expr

def isPair(n: Int): Boolean = {
  n % 2 == 0
}


(streamRange(1000, 10000) filter isPair).toList.take(10)
(streamRange(1000, 10000) filter isPair) apply 2 // Just needed elements from Stream are evaluated

/**
 * Infinite streams
 */
def from(n: Int): Stream[Int] = n #:: from(n+1)

val natural_numbers = from(0)
val x4_multiples = natural_numbers map (_ * 4)

(x4_multiples take 100).toList  // 100 first multiples of 4

// Eratostenes algorithm
def sieve(s: Stream[Int]): Stream[Int] = {
  s.head #:: sieve(s.tail filter (_ % s.head != 0))
}

val primes = sieve(from(2))

primes.take(10).toList

/**
  Water pouring problem
  */
val problem = new Pouring(Vector(4,9))
problem.moves
problem.pathSets.take(3).toList
problem.solution(6)