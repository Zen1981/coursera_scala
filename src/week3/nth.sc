import week3._

def nth[T](n: Int, xs: List[T]): T =
  if (xs.isEmpty) throw new IndexOutOfBoundsException
  else if (n == 0) xs.head
  else nth(n - 1, xs.tail)

val a = 1

// When defining a val with type parameters, scala can infer the type, and this can be omitted
val list1 = new Cons[Int](1, new Nil[Int])
val list2 = new Cons(1, new Cons(2, new Cons(3, new Nil)))

nth(0, list1)
nth(1, list2)
nth(4, list2)