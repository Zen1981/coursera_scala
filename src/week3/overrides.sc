abstract class Base {
  def foo = 1
  def bar: Int
}

class Sub extends Base {
  //def foo = 2 // You can't accidentally override foo
  override def foo = 2

  // You can't override something that doesn't exist in base class
  //override def nonexistent = 2
  def bar = 3
}