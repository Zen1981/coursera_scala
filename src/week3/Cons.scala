package week3

class Cons[T](val head: T, val tail: List[T]) extends List[T] {
  def isEmpty = false

  override def toString = head + "," + tail.toString

  // When val is used for parameters, you actually are defining a member in the class with that name so implicitly we are
  // defining head and tail.
}