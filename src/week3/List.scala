package week3

// T is a type parameter (~generic) and can be used with functions too.
trait List[T] {
  def isEmpty: Boolean

  def head: T

  def tail: List[T]
}



