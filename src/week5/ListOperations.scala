package week5

import math.Ordering

object ListOperations {
  def last[T](xs: List[T]): T = xs match {
    case List() => throw new Error("last of empty list")
    case List(x) => x
    case y :: ys => last(ys)
  }

  // Patterns are matched in sequence, so in 3rd match, we are sure that argument is not an empty list nor a 1 element list
  def init[T](xs: List[T]): List[T] = xs match {
    case List() => throw new Error("init of empty list")
    case List(x) => Nil
    case y :: ys => y :: init(ys)
  }

  // Typically we construct lists from left to right, so it make sense to match on xs
  // Complexity : length(xs)
  def concat_v1[T](xs: List[T], ys: List[T]): List[T] = xs match {
    case List() => ys
    case z :: zs => z :: concat_v1(zs, ys)
  }

  // Complexity : N (reverse) * N (concat)
  def reverse[T](xs: List[T]): List[T] = xs match {
    case List() => xs
    case y :: ys => reverse(ys) ++ List(y)
  }

  // Concat a list without n and another with the rest of the original list
  def removeAt(n: Int, xs: List[Int]) = (xs take n) ::: (xs drop n + 1)

  // Merge sort: algorithm for sorting a list better than insertion sort:
  // - If list is empty, it is already sorted
  // - Otherwise, separate list into two sublists, each with around half of the elements: sort the two sublists and merge them in a single sorted list
  def msort_v1(xs: List[Int]): List[Int] = {
    val n = xs.length / 2 // Division truncates here
    if (n == 0) xs
    else {
      // Matching on a pair
      def merge(xs: List[Int], ys: List[Int]): List[Int] = (xs, ys) match {
        case (Nil, ys) => ys
        case (xs, Nil) => xs
        case (x :: xs1, y :: ys1) =>
          if (x < y) x :: merge(xs1, ys)
          else y :: merge(ys1, xs)
      }
      val (fst, snd) = xs splitAt n
      merge(msort_v1(fst), msort_v1(snd))
    }
  }

  // Improved version for type parameters
  def msort_v2[T](xs: List[T])(lt: (T, T) => Boolean): List[T] = {
    val n = xs.length / 2 // Division truncates here
    if (n == 0) xs
    else {
      // Matching on a pair
      def merge(xs: List[T], ys: List[T]): List[T] = (xs, ys) match {
        case (Nil, ys) => ys
        case (xs, Nil) => xs
        case (x :: xs1, y :: ys1) =>
          if (lt(x, y)) x :: merge(xs1, ys)
          else y :: merge(ys1, xs)
      }
      val (fst, snd) = xs splitAt n
      merge(msort_v2(fst)(lt), msort_v2(snd)(lt))
    }
  }

  // Improved with built-in library in Scala (scala.math.Ordering)
  def msort_v3[T](xs: List[T])(ord: Ordering[T]): List[T] = {
    val n = xs.length / 2 // Division truncates here
    if (n == 0) xs
    else {
      // Matching on a pair
      def merge(xs: List[T], ys: List[T]): List[T] = (xs, ys) match {
        case (Nil, ys) => ys
        case (xs, Nil) => xs
        case (x :: xs1, y :: ys1) =>
          if (ord.lt(x, y)) x :: merge(xs1, ys)
          else y :: merge(ys1, xs)
      }
      val (fst, snd) = xs splitAt n
      merge(msort_v3(fst)(ord), msort_v3(snd)(ord))
    }
  }

  // Improved with implicit parameters
  // When a parameter is implicit,
  //  - The compiler will figure out what to pass based on the demanded type.
  //  - It will look for a visible candidate at the function call (or defined
  //  by a companion object associated with type parameter.
  // Warning: If there are no candidates or more than 1, it will throw an exception
  def msort_v4[T](xs: List[T])(implicit ord: Ordering[T]): List[T] = {
    val n = xs.length / 2 // Division truncates here
    if (n == 0) xs
    else {
      // Matching on a pair
      def merge(xs: List[T], ys: List[T]): List[T] = (xs, ys) match {
        case (Nil, ys) => ys
        case (xs, Nil) => xs
        case (x :: xs1, y :: ys1) =>
          if (ord.lt(x, y)) x :: merge(xs1, ys)
          else y :: merge(ys1, xs)
      }
      val (fst, snd) = xs splitAt n
      merge(msort_v4(fst), msort_v4(snd))
    }
  }

  def scaleList_v1(xs: List[Double], factor: Double): List[Double] = xs match {
    case Nil => xs
    case y :: ys => y * factor :: scaleList_v1(ys, factor)
  }

  // Improved using map operation
  def scaleList_v2(xs: List[Double], factor: Double): List[Double] = xs map (x => x * factor)

  def squareList_v1(xs: List[Int]): List[Int] = xs match {
    case Nil => Nil
    case y :: ys => y * y :: squareList_v1(ys)
  }

  def squareList_v2(xs: List[Int]): List[Int] = xs map (x => x * x)

  def posElems_v1(xs: List[Int]): List[Int] = xs match {
    case Nil => xs
    case y :: ys => if (y > 0) y :: posElems_v1(ys) else posElems_v1(ys)
  }

  // Improved using filter operation
  def posElems_v2(xs: List[Int]): List[Int] = xs filter (x => x > 0)

  def pack[T](xs: List[T]): List[List[T]] = xs match {
    case Nil => Nil
    case x :: xs1 =>
      val (first, rest) = xs span (y => y == x)
      first :: pack(rest)
  }

  def encode_v1[T](xs: List[T]): List[(T, Int)] = xs match {
    case Nil => Nil
    case z :: zs =>
      val (first, last) = xs span (y => y == z)
      List((z, first.length)) ::: encode_v1(last)
  }

  // Improved using pack
  def encode_v2[T](xs: List[T]): List[(T, Int)] = pack(xs) map (ys => (ys.head, ys.length))

  def sum_v0(xs: List[Int]): Int =
    if (xs.isEmpty) 0
    else xs.head + sum_v0(xs.tail)

  def sum_v1(xs: List[Int]): Int = xs match {
    case Nil => 0
    case y :: ys => y + sum_v1(ys)
  }

  // Improve using reduceLeft
  def sum_v2(xs: List[Int]): Int = xs reduceLeft ((x, y) => x + y)

  // Improve using syntactic sugar
  def sum_v3(xs: List[Int]): Int = xs reduceLeft (_ + _)

  def prod(xs: List[Int]): Int = xs reduceLeft (_ * _)

  // Improved using foldRight
  // Warning, foldLeft can't be used on this case
  def concat_v2[T](xs: List[T], ys: List[T]): List[T] = (xs foldRight ys)(_ :: _)

  def max(xs: List[Int]): Int = {
    if (xs.isEmpty) throw new java.util.NoSuchElementException
    else
    if (xs.tail.isEmpty) xs.head
    else xs reduceLeft ((x,y) => returnMax(x,y) )
  }

  def returnMax(a: Int, b: Int) =
    if (a>b) a
    else b
}
