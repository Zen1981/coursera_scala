Lists

    * Construction (standard)
        · List(elem1, elem2, ...)
        ! Can be nested (list of lists)
    * Features
        · Lists are immutable in scala
        · Lists are recursive, not flat like arrays
    * Structure
        · Object List in library has a head (first element) and a tail (pointer to the rest of the list)
        · Has 3 operations : head, tail and isEmpty
        · Exceptions are thrown if we try to access to head or tail of empty list
    * Homogeneus
        · All elements in a list must be of the SAME type
    * Syntactic sugar
        · The empty list is the same as 'Nil'
        · Construction operator (::) aka 'cons'. Can be seen as the prepend operator

            x :: xs     is equivalent to    List(x, <rest of elements>)
            1 :: (2 :: (3 :: (4 :: Nil)))

            ! Convention for scala : operators ending in ':' associate to the right, so parenthesis can be ommited

            1 :: 2 :: 3 :: 4 :: Nil

            ! Plus, they are seen as method calls of the right-hand operand (on the contrary of the common operators)

            Nil.::(4).::(3).::(2).::(1)
    * Pattern matching
        · Nil
        · p :: ps                   A list with head matching p and tail ps (arbitrary)
        · List(p1, ... , pn)        same as p1 :: ... :: pn :: Nil
        · x :: Nil                  lists of length 1
        · List()                    same as Nil
        · List(2 :: xs)             List containing only an element, which in turn is a list that starts with 2
    * Operations (0 indexed)
        · xs.length                 Number of elements
        · xs.last                   Retrieves the last element
        · xs.init                   Retrieves all elements except the last
        · xs take n                 Retrieves the first n elements (or xs if length < xs)
        · xs drop n                 Rest of the collection after taking n elements
        · xs(n)                     Element at index n (0-indexed!)
        · xs ++ ys                  Concatenation in a new list
        · xs.reverse                Gets a reversed list
        · xs updated (n,x)          Get a list where index n is replaced with x
        · xs indexOf x              Index of first element equal to x or -1
        · xs contains x             Same as [xs indexOf x >= 0]
        · xs ::: ys                 Concatenation of lists
        · xs splitAt n              Return two lists on a pair object
        · xs map (f: x => f(x))         Return a list that is transformed by the specified function
        · xs filter (p: T => Boolean)   Return a list that is filtered by the specified predicate
        · xs filterNot (p: T => Boolean)
        · xs partition (p: T => Boolean) : (List[T],List[T])
        · xs takeWhile (p: T => Boolean)
        · xs dropWhile (p: T => Boolean)
        · xs span (p: T => Boolean) : (List[T],List[T])
        · xs reduceLeft (f: (x,y) => f(x,y))
            ! Can only be applied to non empty lists
        · xs foldLeft
            · Generalization for reduceLeft, can be used also with non empty lists
            · Uses an accumulator as an additional parameter, that is used in first iteration
        · xs reduceRight
        · xs foldRight

