import week5.ListOperations._

val list0 = Nil
val list1 = List(1)
val list2 = List(8, 1)
val list3 = List(6, 8, 1)
val list4 = List(7, -3, 2, 1)
val listString = List("apple", "pineapple", "orange", "banana")
val listDouble = List(1.0, 2.0, 3.0, 4.0)
val listDuplicated = List("a", "a", "a", "b", "c", "c", "a")

//ListOperations.last(list0)
last(list1)
last(list2)

//ListOperations.init(list0)
init(list1)
init(list2)

concat_v1(list0, list2)
concat_v1(list1, list2)

reverse(list2)

removeAt(1, List(9, 8, 7, 6))

msort_v1(list4)
msort_v2(list4)((x:Int, y:Int) => x < y)
msort_v2(listString)((x:String, y:String) => x.compareTo(y) < 0)

// Types can be inferred by scala. But to ensure this, parameterized functions must be at the end of the argument list.
msort_v2(list4)((x, y) => x < y)
msort_v2(listString)((x, y) => x.compareTo(y) < 0)

msort_v3(listString)(Ordering.String)

msort_v4(listString)

scaleList_v1(listDouble, 2)
scaleList_v2(listDouble, 2)

squareList_v1(list2)
squareList_v2(list2)

posElems_v1(list4)
posElems_v2(list4)

// Other operations for high order functions on lists
list4 filter (x => x > 0)
list4 filterNot (x => x > 0)
list4 partition (x => x > 0)

list4 takeWhile (x => x > 0)
list4 dropWhile (x => x > 0)
list4 span (x => x > 0)

pack(listDuplicated)
encode_v1(listDuplicated)
encode_v2(listDuplicated)

sum_v1(list3)
sum_v2(list3)
sum_v3(list3)

prod(list3)

concat_v2(list2,list3)

list2.foldRight(list3)(_ :: _)

max(List(2,1,9,5,8))
max(List(1))

sum_v0(List(1,2,3,4))
sum_v0(List())
sum_v0(List(1))