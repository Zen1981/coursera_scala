package week6

object polynomials_v2 {

  class Poly(val terms0: Map[Int, Double]) {
    val terms = terms0 withDefaultValue 0.0 // This avoid to be forced to do pattern matching on adjust function

    def + (other: Poly) = new Poly(terms ++ (other.terms map adjust)) // If we don't map with adjust, the term in "terms" will be replaced by other's one

    /**
     * Recompute term adding the existing coeff in the current poly if exists
     * @param term
     * @return
     */
    def adjust(term: (Int, Double)): (Int, Double) = {
      val (exp, coeff) = term
      exp -> (coeff + terms(exp))
    }

    override def toString =
      (for ((exp, coeff) <- terms.toList.sorted.reverse) yield coeff + "x^" + exp) mkString " + "
  }
}
