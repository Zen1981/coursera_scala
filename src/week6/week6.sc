import week6.PhoneMnemonics
import week6.polynomials_v3.Poly

// Map
val xs = Array(1, 2, 3, 44)
xs map (x => x * 2)

// Filter
val str = "Hello World"
str filter (c => c.isUpper)

// Ranges
val r: Range = 1 until 5
val s: Range = 1 to 5
1 to 10 by 3
6 to 1 by -2

// exists and forall
str exists (c => c.isUpper)
str forall (c => c.isUpper)

// zip and unzip
val pairs = List(1, 2, 3) zip str
pairs.unzip

// flatMap
str flatMap (c => List('.', c))

// sum, product, max, min
s.sum
s.product
s.max
s.min

val v1 = Vector(1.0, 2.0, 3.0, 4.0)
val v2 = Vector(4.0, 3.0, 2.0, 1.0)

// Scalar product (basic version)
def scalarProduct_v1(xs: Vector[Double], ys: Vector[Double]): Double = (xs zip ys).map(xy => xy._1 * xy._2).sum
scalarProduct_v1(v1, v2)

// Scalar product (with pattern matching function)
def scalarProduct_v2(xs: Vector[Double], ys: Vector[Double]): Double = (xs zip ys).map {
  case (x, y) => x * y
}.sum
scalarProduct_v2(v1, v2)

def isPrime(n: Int): Boolean =
  (2 until n) forall (d => n % d != 0)

isPrime(56)

// Find all positive numbers such as they sum a prime number (way 1)
// flatten operation is equivalent to xss foldRight Seq[Int]())(_ ++ _)
def sumIsPrime_v1(n: Int) =
  ((1 until n) map (i =>
    (1 until i) map (j =>
      (i, j)
      )
    )).flatten filter (pair => isPrime(pair._1 + pair._2))

sumIsPrime_v1(5)

// xs flatMap f = (xs map f).flatten
def sumIsPrime_v2(n: Int) =
  (1 until n) flatMap (i =>
    (1 until i) map (j =>
      (i, j)
      )
    ) filter (pair => isPrime(pair._1 + pair._2))
sumIsPrime_v2(5)

// Use of for expressions
def sumIsPrime_v3(n: Int) =
  for {
    i <- 1 until n
    j <- 1 until i
    if isPrime(i + j)
  } yield (i, j)
sumIsPrime_v3(5)

// Scalar product (using for-expression)
def scalarProduct_v3(xs: Vector[Double], ys: Vector[Double]): Double =
  (for {
    (x, y) <- xs zip ys
  } yield (x * y)).sum

scalarProduct_v3(v1, v2)

// Polynomials
val p1 = new Poly(1 -> 2.0, 3 -> 4.0, 5 -> 6.2)
val p2 = new Poly(0 -> 3.0, 3 -> 7.0)
p1 + p2
p1.terms(7)

// Phone mnemonics
PhoneMnemonics.translate("7225247386")