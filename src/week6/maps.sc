// Types can be ommited, infered by scala
val romanNumerals = Map[String, Int]("I" -> 1, "V" -> 5, "X" -> 10)
romanNumerals("I")
romanNumerals get "C"

// Pattern matching with get and Option object
def getDecimalValue(romanValue: String) = romanNumerals.get(romanValue) match {
  case Some(value) => value
  case None => "Missing key"
}

getDecimalValue("I")
getDecimalValue("C")

// Collection operations
val list = List("apple","pear","orange","pineapple")
list sortWith(_.length < _.length)
list.sorted
list.groupBy(_.head)