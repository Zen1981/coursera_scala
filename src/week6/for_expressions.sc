case class Book(title: String, authors: List[String])

// Named parameters in constructors can be used to increase readability
val books: List[Book] = List(
  new Book(
    title = "Structure and Interpretation of Computer Programs",
    authors = List("Abelson, Harald", "Sussman, Gerald J.")
  ),
  new Book(
    title = "Introduction to Functional Programming",
    authors = List("Bird, Richard", "Wadler, Phil")
  ),
  new Book(
    title = "Effective Java",
    authors = List("Bloch, Joshua")
  ),
  new Book(
    title = "Java Puzzlers",
    authors = List("Bloch, Joshua", "Gafter, Neal")
  ),
  new Book(
    title = "Programming in Scala",
    authors = List("Odersky, Martin", "Spoon, Lex", "Venners, Bill")
  )
)

for (b <- books; a <- b.authors if a startsWith "Bird,")
  yield b.title

// Curly braces increase readability and avoid the use of ";"
for {
  b <- books
  if (b.title indexOf "Program") >= 0
} yield b.title

for {
  b <- books
  a <- b.authors
  if a startsWith "Bloch,"
} yield b.title

// Authors with more than 1 book
for {
  b1 <- books
  b2 <- books
  if b1 != b2
  a1 <- b1.authors
  a2 <- b2.authors
  if a1 == a2
} yield a1

// Authors with more than 1 book (Avoiding repetitions)
// Order doesn't matter
for {
  b1 <- books
  b2 <- books
  if b1.title < b2.title
  a1 <- b1.authors
  a2 <- b2.authors
  if a1 == a2
} yield a1


// Authors with more than 1 book (Avoiding repetitions)
// Using distinct duplicate pairs are removed
{
  for {
    b1 <- books
    b2 <- books
    if b1 != b2
    a1 <- b1.authors
    a2 <- b2.authors
    if a1 == a2
  } yield a1
}.distinct

// Understanding map, flatMap and filter
val list1 = List("a", "b", "c")
list1 map (x => x + "f")
list1 filter (x => x > "b")
list1 flatMap (x => List(x + 1,x + 2,x + 3))

