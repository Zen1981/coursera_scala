package week6

object polynomials_v1 {

  class Poly(val terms: Map[Int, Double]) {
    def + (other: Poly) = new Poly(terms ++ (other.terms map adjust)) // If we don't map with adjust, the term in "terms" will be replaced by other's one

    /**
     * Recompute term adding the existing coeff in the current poly if exists
     * @param term
     * @return
     */
    def adjust(term: (Int, Double)): (Int, Double) = {
      val (exp, coeff) = term
      terms get exp match {
        case Some(coeff1) => exp -> (coeff + coeff1)
        case None => exp -> coeff
      }
    }


    override def toString =
      (for ((exp, coeff) <- terms.toList.sorted.reverse) yield coeff + "x^" + exp) mkString " + "
  }
}
