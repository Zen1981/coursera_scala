package week6

import scala.io.Source

/**
 * Phone keys have mnemonics assigned to them:
 * '2' -> "ABC"
 * '3' -> "DEF"
 * '4' -> "GHI"
 * '5' -> "JKL"
 * '6' -> "MNO"
 * '7' -> "PQRS"
 * '8' -> "TUV"
 * '9' -> "WXYZ
 *
 * From a given dictionary, design a method translate such that a phone number produces all possible words that are
 * mnemonics.
 */
//


object PhoneMnemonics {
  val in = Source.fromFile("D:\\temp\\git\\scala_coursera\\src\\week6\\phone_mnemonics_dictionary.txt")

  // We filter words that contains other characters than letters
  val words = in.getLines().toList filter (word => word forall (chr => chr.isLetter))

  val mnem = Map(
    '2' -> "ABC",
    '3' -> "DEF",
    '4' -> "GHI",
    '5' -> "JKL",
    '6' -> "MNO",
    '7' -> "PQRS",
    '8' -> "TUV",
    '9' -> "WXYZ"
  )

  /** Invert the mnem map to give a map from chars to numbers */

  // Invalid way : violates DRY principle
  // val charCode : Map[Char, Char] = Map('A' -> '2', 'B' -> '2', ...)

  val charCode : Map[Char, Char] = for {
    (digit, str) <- mnem;
    ltr <- str
  } yield ltr -> digit

  /** Maps a word to the digit String it can represent, e.g. "Java" -> "5282" */
  private def wordCode(word: String): String = word.toUpperCase map charCode

  /** A map from digit strings to the words that represent them.
    * e,g. "5282" -> List("Java", "Kata", "Lava", ...)
    * Note: A missing number should map to the empty set, e.g. "1111" -> List()
    * */
  val wordsForNum: Map[String, Seq[String]] =
    words groupBy wordCode withDefaultValue Seq()

  /** Return all ways to encode a number as a list of words */
  private def encode(number: String): Set[List[String]] =
    if (number.isEmpty) Set(List())
    else {
      for {
        split <- 1 to number.length             // Generate from 1 to length of number
        word <- wordsForNum(number take split)  // Store substrings of all lengths in 'word'
        rest <- encode(number drop split)       // Recursive call with the rest of the string
      } yield word :: rest
    }.toSet

  /** Convert every solution thrown by encode into a flat set of strings */
  def translate(number: String): Set[String] =
    encode(number) map (_ mkString " ")
}
