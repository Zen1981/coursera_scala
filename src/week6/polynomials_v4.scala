package week6

object polynomials_v4 {

  class Poly(val terms0: Map[Int, Double]) {
    // We define a new constructor that accept a vararg of pairs (expressed with *)
    def this(bindings: (Int, Double)*) = this(bindings.toMap)

    val terms = terms0 withDefaultValue 0.0 // This avoid to be forced to do pattern matching on adjust function

    // + operator can also be defined with a foldLeft operation (more efficient as does not create intermediate structures)
    def + (other: Poly) = new Poly((other.terms foldLeft terms)(addTerm)) // If we don't map with adjust, the term in "terms" will be replaced by other's one

    def addTerm(terms: Map[Int,Double], term: (Int,Double)): Map[Int, Double] = {
      val (exp, coeff) = term
      terms + (exp -> (coeff + terms(exp)))
    }


    /**
     * Recompute term adding the existing coeff in the current poly if exists
     * @param term
     * @return
     */
    def adjust(term: (Int, Double)): (Int, Double) = {
      val (exp, coeff) = term
      exp -> (coeff + terms(exp))
    }

    override def toString =
      (for ((exp, coeff) <- terms.toList.sorted.reverse) yield coeff + "x^" + exp) mkString " + "
  }
}
