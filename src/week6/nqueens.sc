// We will develop an algorithm to find a way of putting a set of queens
// (chess figure) along a chessboard of size NxN. No queen must threaten another
// Solutions will be given as a Set of Lists, in which each list represents a solution
// The solution implicitly take the position of element as the row (inversed), and the value of the element as the column

def queens(n: Int): Set[List[Int]] = {

  def placeQueens(k: Int): Set[List[Int]] =
    if (k == 0) Set(List())
    else
      for {
        queens <- placeQueens(k - 1)
        col <- 0 until n
        if isSafe(col, queens)
      } yield col :: queens

  placeQueens(n)
}

def isSafe(col: Int, queens: List[Int]): Boolean = {
  val row = queens.length
  val queensWithRow = (row - 1 to 0 by -1) zip queens
  queensWithRow forall {
    case (r,c) =>
      col != c &&
        math.abs(col-c) != row - r
  }
}

def show(queens: List[Int]) = {
  val lines =
    for (col <- queens.reverse) yield
    Vector.fill(queens.length)("* ").updated(col, "X ").mkString
  "\n" + (lines mkString "\n")
}

(queens(4) map show) mkString "\n"

// mkString is a handy operation for collections that take all elements and store them sequentially in a String, optionally separated by a character (optional second argument of operation)

(queens(8) take 3 map show) mkString "\n"


