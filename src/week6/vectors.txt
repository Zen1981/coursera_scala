Vector
    * Implementation of sequences (Seq)
    * Initially it has a preallocated length of 32 (2^5)
    * If elements grow larger than 32, first level becomes references to blocks of 32 elements, so in total there are 2^10 elements
    * For nth level, it will exist 2^(n*5)
    * The depth of the vector is log(32)N
    * Very good for bulk operations (map, filter, ...)
    * Good locality, better than lists
    * Analogous syntax in comparation with Lists
        - val nums = Vector(1,2,3)
        - :: does not exist for Vectors. Instead we have +: and :+ (colon always pointing to the collection)
        - Most operations within Lists are available also in Vectors
    
